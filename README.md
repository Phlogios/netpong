# NetPong
A networked multiplayer version of Pong, written using Unity3D and Photon Unity Networking

## How to play
Controls: W and S

## Known bugs
- Points can be added without the ball actually exiting the field in high-latency scenarios
- Ball jitters when ownership is transferred

## Postmortem
This is how far I came.
I spent around 6 hours total researching and implementing the game.
It was a great learning experience, and I think I would do things differently if I did it again.
For starters, while syncing transforms works fine on the paddles, it doesn't work as well on the ball.
To tackle latency issues, I transferred the ownership of the ball when it changed horizontal direction.
It helped with collision checking, but it didn't look nice, and ruined the Pong feeling.
A better solution would be to keep the ball updates client-side,
and use RPC to set a new velocity vector and bounce position when the ball hits a paddle.
I would do the same thing with score - let the each client keep track of when the ball exits the field on his side, and send score updates to the other player.
Sadly, I ran out of time before I could start working on the asset replacement feature. My plan for implementing that feature would be to let the client download textures from a server before showing the Launcher menu.

If I had more time, I would have spent some time setting up automated tests. It was cumbersome to launch two clients every time I wanted to test the game.

Some of the networking code has been copied from the Photon documentation.

Gustav Pihl Bohlin
