﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour {
	public static Scoreboard Instance = null;

	public Text player1ScoreText;
	public Text player2ScoreText;

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		SetScore ();
	}

	public void SetScore() {
		int player1Score = 0;
		int player2Score = 0;

		foreach (var player in PhotonNetwork.playerList) {
			if (player.ID == 1)
				player1Score = player.GetScore ();
			else
				player2Score = player.GetScore ();
		}

		player1ScoreText.text = player1Score.ToString();
		player2ScoreText.text = player2Score.ToString();
	}
}
