﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiplayerGameManager : Photon.PunBehaviour {
	public GameObject playerPrefab;
	public GameObject ballPrefab;

	float localPlayerX = -5.0f;
	GameObject localPlayer = null;

	// Use this for initialization
	void Start () {
		Debug.Log("We are Instantiating LocalPlayer from " + SceneManager.GetActiveScene().name);
		// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
		if (PhotonNetwork.player.ID != 1) {
			localPlayerX = -localPlayerX;
		}

		if (PaddleController.LocalPlayerInstance==null)
		{
			Debug.Log("We are Instantiating LocalPlayer from "+SceneManager.GetActiveScene().name);
			// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
			if(!localPlayer)
				localPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(localPlayerX, 0f, 0f), Quaternion.identity, 0);
		}else{
			Debug.Log("Ignoring scene load for " + SceneManager.GetActiveScene().name);
		}

		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.Instantiate (this.ballPrefab.name, new Vector3 (0f, 0f, 0f), Quaternion.identity, 0);
		}
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnLeftRoom()
	{
		SceneManager.LoadScene(0);
	}

	public void LeaveRoom()
	{
		PhotonNetwork.LeaveRoom();
	}

	void LoadArena()
	{
		if ( ! PhotonNetwork.isMasterClient ) 
		{
			Debug.LogError( "PhotonNetwork : Trying to Load a level but we are not the master Client" );
			return;
		}
		Debug.Log( "PhotonNetwork : Loading Level : " + PhotonNetwork.room.PlayerCount );
		PhotonNetwork.LoadLevel("Online Room for "+PhotonNetwork.room.PlayerCount);
	}

	public override void OnPhotonPlayerConnected( PhotonPlayer other  )
	{
		Debug.Log( "OnPhotonPlayerConnected() " + other.NickName ); // not seen if you're the player connecting

		if ( PhotonNetwork.isMasterClient ) 
		{
			Debug.Log( "OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient ); // called before OnPhotonPlayerDisconnected

			LoadArena();
		}
	}

	public override void OnPhotonPlayerDisconnected( PhotonPlayer other  )
	{
		Debug.Log( "OnPhotonPlayerDisconnected() " + other.NickName ); // seen when other disconnects

		if ( PhotonNetwork.isMasterClient ) 
		{
			Debug.Log( "OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient ); // called before OnPhotonPlayerDisconnected

			PaddleController.LocalPlayerInstance.transform.position = new Vector3 (-5.0f, 0f, 0f);
			LoadArena();
		}
	}
}
