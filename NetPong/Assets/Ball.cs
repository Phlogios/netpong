﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : Photon.PunBehaviour, IPunObservable {
	public float size = 0.25f;
	Vector3 velocity;
	// Use this for initialization
	void Start () {
		velocity = new Vector2 (-2, 0);
		Reset ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += velocity*Time.deltaTime;
		if (photonView.isMine) {
			//Ceiling and floor bounce check
			var position = transform.position;
			if (position.y > Camera.main.orthographicSize - size * 0.5f) {
				position.y = Camera.main.orthographicSize - size * 0.5f;
				velocity.y = -Mathf.Abs (velocity.y);
			} else if (position.y < -Camera.main.orthographicSize + size * 0.5f) {
				position.y = -Camera.main.orthographicSize + size * 0.5f;
				velocity.y = Mathf.Abs (velocity.y);
			}

			//Check if outside area

			if (position.x > 6.0f) {
				if (PhotonNetwork.player.ID == 1)
					PhotonNetwork.player.AddScore (1);
				else
					PhotonNetwork.otherPlayers [0].AddScore (1);
				Reset ();
			} else if (position.x < -6.0f) {
				if (PhotonNetwork.player.ID == 2)
					PhotonNetwork.player.AddScore (1);
				else
					PhotonNetwork.otherPlayers [0].AddScore (1);
				Reset ();
			}
		}
	}

	public void Reset() {
		velocity.y = Random.Range (-0.5f, 0.5f);
		velocity = velocity.normalized*3;
		transform.position = new Vector2 (0.0f, 0.0f);

		TransferOwnerIfBallIsFlyingAwayFromMe ();
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.transform.position.x < 0)
			velocity.x = Mathf.Abs (velocity.x);
		else
			velocity.x = -Mathf.Abs (velocity.x);

		var paddle = collider.GetComponent<Paddle> ();
		if(paddle)
			velocity.y += (transform.position.y - collider.transform.position.y);

		TransferOwnerIfBallIsFlyingAwayFromMe ();
	}

	void TransferOwnerIfBallIsFlyingAwayFromMe() {
		if (photonView.isMine) {
			var myPosition = PaddleController.LocalPlayerInstance.transform.position.x;
			bool ballFlyingTowardsMe = (myPosition > 0 && velocity.x > 0) || (myPosition < 0 && velocity.x < 0);
			if(!ballFlyingTowardsMe)
				photonView.TransferOwnership(PhotonNetwork.otherPlayers[0]);
		}
	}

	#region IPunObservable implementation
	void IPunObservable.OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			// We own this ball: send the others our data
			stream.SendNext(velocity);
		}else{
			// Network player, receive data
			this.velocity = (Vector3)stream.ReceiveNext();
		}
	}
	#endregion
}
