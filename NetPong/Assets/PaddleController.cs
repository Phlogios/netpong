﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : Photon.PunBehaviour {
	public static GameObject LocalPlayerInstance = null;
	float movementAxis = 0.0f;
	public float movementSpeed = 0.01f;
	public int playerId = 1;
	string axis;

	Paddle paddle;

	void Awake() {
		if ( photonView.isMine)
		{
			PaddleController.LocalPlayerInstance = this.gameObject;
			playerId = photonView.ownerId;
			axis = "Vertical1";
		}
		DontDestroyOnLoad(this.gameObject);
	}

	// Use this for initialization
	void Start () {
		paddle = GetComponent<Paddle> ();
	}
	
	// Update is called once per frame
	void Update () {
		if( photonView.isMine == false && PhotonNetwork.connected == true )
		{
			return;
		}
		ProcessInputs ();
	}

	void ProcessInputs() {
		movementAxis = Input.GetAxis (axis);
	}

	void FixedUpdate() {
		var position = transform.position;
		position.y += movementAxis * movementSpeed;

		if (position.y > Camera.main.orthographicSize - paddle.height * 0.5f) {
			position.y = Camera.main.orthographicSize - paddle.height * 0.5f;
		} else if (position.y < -Camera.main.orthographicSize + paddle.height * 0.5f) {
			position.y = -Camera.main.orthographicSize + paddle.height * 0.5f;
		}

		transform.position = position;
	}
}