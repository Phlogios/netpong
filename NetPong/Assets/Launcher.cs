﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Launcher : Photon.PunBehaviour {
	string _gameVersion = "1";
	public PhotonLogLevel Loglevel = PhotonLogLevel.Informational;
	public byte MaxPlayersPerRoom = 2;
	bool isConnecting;

	void Awake()
	{
		PhotonNetwork.logLevel = Loglevel;
		// #Critical
		// we don't join the lobby. There is no need to join a lobby to get the list of rooms.
		PhotonNetwork.autoJoinLobby = false;

		// #Critical
		// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
		PhotonNetwork.automaticallySyncScene = true;
	}

	// Use this for initialization
	void Start () {
	}

	public void Connect()
	{
		isConnecting = true;
		// we check if we are connected or not, we join if we are , else we initiate the connection to the server.
		if (PhotonNetwork.connected)
		{
			// #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
			PhotonNetwork.JoinRandomRoom();
		}else{
			// #Critical, we must first and foremost connect to Photon Online Server.
			PhotonNetwork.ConnectUsingSettings(_gameVersion);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//Photon
	public override void OnConnectedToMaster()
	{
		Debug.Log("NetPong: OnConnectedToMaster() was called by PUN");
		if (isConnecting)
		{
			// #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnPhotonRandomJoinFailed()
			PhotonNetwork.JoinRandomRoom();
		}
	}

	public override void OnDisconnectedFromPhoton()
	{
		Debug.LogWarning("NetPong: OnDisconnectedFromPhoton() was called by PUN");        
	}

	public override void OnPhotonRandomJoinFailed (object[] codeAndMsg)
	{
		Debug.Log("NetPong:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.");

		// #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
		PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = MaxPlayersPerRoom }, null);
	}

	public override void OnJoinedRoom()
	{
		Debug.Log("NetPong: OnJoinedRoom() called by PUN. Now this client is in a room.");
		if (PhotonNetwork.room.PlayerCount == 1)
		{
			Debug.Log("We load the 'Online Room for 1' ");

			// #Critical
			// Load the Room Level. 
			PhotonNetwork.LoadLevel("Online Room for 1");
		}
	}
}
