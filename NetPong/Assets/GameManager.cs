﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public GameObject playerPrefab;
	public GameObject ballPrefab;

	// Use this for initialization
	void Start () {
		var player1 = GameObject.Instantiate (playerPrefab, new Vector3 (-5.0f, 0, 0), Quaternion.identity);
		player1.GetComponent<PaddleController> ().playerId = 1;

		if (PlayerPrefs.GetInt ("numPlayers") > 1) {
			var player2 = GameObject.Instantiate (playerPrefab, new Vector3 (5.0f, 0, 0), Quaternion.identity);
			player2.GetComponent<PaddleController> ().playerId = 2;
		}

		GameObject.Instantiate (ballPrefab, new Vector3 (0, 0, 0), Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
